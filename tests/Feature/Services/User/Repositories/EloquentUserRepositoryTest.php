<?php

namespace Tests\Feature\Services\User\Repositories;

use App\Services\User\Repositories\EloquentUserRepository;
use Tests\TestCase;

class EloquentUserRepositoryTest extends TestCase
{
    private function getEloquentUserRepository(): EloquentUserRepository
    {
        return app(EloquentUserRepository::class);
    }

    public function testFindExpectsNull(): void
    {
        $user = $this->getEloquentUserRepository()->find(1234);
        $this->assertNull($user);
    }
}
