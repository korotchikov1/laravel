<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Users\Requests\UpdateUserRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UpdateUserController extends BaseUserController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(UpdateUserRequest $request, int $id): JsonResponse
    {
        try {
            $user = $this->getUserService()->find($id);
            $user = $this->getUserService()->update($user, $request->validated());
            return $this->goodResponse($user);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'User not found'], 404);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->validator->getMessageBag()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
