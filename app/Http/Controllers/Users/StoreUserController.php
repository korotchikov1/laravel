<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Users\Requests\StoreUserRequest;
use Illuminate\Http\Request;

class StoreUserController extends BaseUserController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(StoreUserRequest $request)
    {
        $user = $this->getUserService()->store($request->validated());
        return $this->goodResponse($user);
    }
}
