<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShowUserController extends BaseUserController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(int $id)
    {
        $user = $this->getUserService()->find($id);
        return $this->goodResponse($user);
    }
}
