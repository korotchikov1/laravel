<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeleteUserController extends BaseUserController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(int $id)
    {
        $this->getUserService()->delete($id);
        return $this->goodResponse([]);
    }
}
