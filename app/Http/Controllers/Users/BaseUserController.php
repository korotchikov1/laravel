<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Services\User\AuthService;
use App\Services\User\UserService;

abstract class BaseUserController extends Controller
{
    protected function getUserService(): UserService
    {
        return app(UserService::class);
    }

    protected function getAuthService(): AuthService
    {
        return app(AuthService::class);
    }
}
