<?php

namespace App\Http\Controllers\Users\Requests;

use App\Http\BaseFormRequest;

class StoreUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'string',
            'phone' => 'string',
            'password' => 'string',
        ];
    }
}
