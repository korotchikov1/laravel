<?php

namespace App\Http\Controllers\Users\Requests;

use App\Http\BaseFormRequest;

class UpdateUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'phone' => 'string',
        ];
    }
}
