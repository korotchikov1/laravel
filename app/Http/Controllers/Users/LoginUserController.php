<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Users\Requests\LoginRequest;

class LoginUserController extends BaseUserController
{
    public function __invoke(LoginRequest $request)
    {
        $jwt = $this->getAuthService()->login($request->validated('email'), $request->validated('password'));
        return $this->goodResponse((object)['token' => $jwt]);
    }
}
