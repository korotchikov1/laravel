<?php

namespace App\Services\User;

use App\Exceptions\UserNotFoundException;
use App\Services\User\Repositories\UserRepository;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    public function login(string $email, string $password): JWTAuth|bool
    {
        $user = $this->userRepository->checkCredentials($email, $password);
        if ($user) {
            return JWTAuth::fromUser($user);
        }
        throw new UserNotFoundException();
    }
}
