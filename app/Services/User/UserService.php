<?php

namespace App\Services\User;

use App\Models\User;
use App\Services\User\Repositories\UserRepository;

class UserService
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    public function find(int $id): User
    {
        return $this->userRepository->find($id);
    }

    public function store(array $data): User
    {
        return $this->userRepository->store($data);
    }

    public function update(User $user, array $data): User
    {
        return $this->userRepository->update($user, $data);
    }

    public function delete(int $id): void
    {
        $this->userRepository->delete($id);
    }
}
