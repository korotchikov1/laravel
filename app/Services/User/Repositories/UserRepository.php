<?php

namespace App\Services\User\Repositories;

use App\Models\User;

interface UserRepository
{
    public function find(int $id) : User;

    public function store(array $data): User;

    public function update(User $user, array $data): User;

    public function delete(int $id): void;
}
