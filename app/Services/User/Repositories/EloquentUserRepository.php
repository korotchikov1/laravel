<?php

namespace App\Services\User\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class EloquentUserRepository implements UserRepository
{

    public function find(int $id): User
    {
        return User::query()->find($id)->first();
    }

    public function store(array $data): User
    {
        $data['password'] = Hash::make($data['password']);
        return User::create($data);
    }

    public function update(User $user, array $data): User
    {
        $user->fill($data)->save();

        return $user;
    }

    public function delete(int $id): void
    {
        User::destroy($id);
    }

    public function findByEmail(string $email): User
    {
        return User::where('email', $email)->first();
    }

    public function checkCredentials(string $email, string $password): bool|User
    {
        $user = $this->findByEmail($email);

        if (!$user || !Hash::check($password, $user->password)) {
            return false;
        }

        return $user;
    }
}
