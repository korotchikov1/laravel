<?php

use App\Http\Controllers\Users\DeleteUserController;
use App\Http\Controllers\Users\LoginUserController;
use App\Http\Controllers\Users\ShowUserController;
use App\Http\Controllers\Users\StoreUserController;
use App\Http\Controllers\Users\UpdateUserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users/{user}', ShowUserController::class);
Route::delete('/users/{user}', DeleteUserController::class);
Route::put('/users/{user}', UpdateUserController::class);
Route::post('/users', StoreUserController::class);
Route::post('/login', LoginUserController::class);
